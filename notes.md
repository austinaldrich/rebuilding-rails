Notes

Rails Libraries:
- ActiveSupport - lots of utility functions, including time and date support or
  inflection.

- ActiveModel - how Rails handles persistence and models, but doesn't require persistence use a database. Ex: if you want a URL for a given model, it will do that. Agnostic to persistence layer.

- ActiveRecord - ORM. Implements ActiveModel.

- ActionPack - does the routing. Maps incoming URLs to controller actions in Rails. Sets up controllers and views. Uses a lot of Rack.

- ActionView - renders template files which eventually become final HTML. Also handles action or view-centered functionality like view caching.

- ActionMailer - used to send out email.

- ActiveJob - for job enqueing. Compatiblity layer around other gems such as Resque, Sidekiq, DelayedJob.

- ActionCable - sets up persistant connection between a rendered web page and your server. Mnages multi-request connection for your Rails app.

